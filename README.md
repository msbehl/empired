# README #

This README documents the steps necessary to get this application up and running.

### What is this repository for? ###

* This repository contains source code of Empired Coding Challenge
* It contains:
- The Database and 
- An MVC Application

### How do I get set up? ###

* Database Setup*
- Open Sql Server Management Studio and attach the database included in the repository

* Application Setup*
- Open the solution in VS 2015
- Ensure that Empired.Games.GuessTheAnimal.WebClient is setup as the StartUp project
- Run the solution (Preferably in Chrome or WebKit based browser)

### Contribution guidelines ###

* Due to lack of time, certain architectural issues haven't been addressed, such as:
- Logging
- Dependency Injection
- Data Contracts for seperation of concerns
- Web service (REST/SOAP) to expose engine functionality
- Web Client using the Web Service to pull & push data instead of directly instantiating the repository
- More tests 

### Who do I talk to? ###

In case of any questions, please get in touch.
* Repo owner or admin
* Other community or team contact