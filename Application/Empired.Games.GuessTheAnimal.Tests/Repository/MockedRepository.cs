﻿using Empired.Games.GuessTheAnimal.IRepository;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empired.Games.GuessTheAnimal.Tests.Repository
{
    // Use mocking frameworks Rhino Mocks/Moq/NMock time permitting
    public class MockAnimalRepository : IRepository<Animal>
    {
        public int Create(Animal entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Animal entity)
        {
            throw new NotImplementedException();
        }

        public void DeleteAll()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Animal> GetAll()
        {
            throw new NotImplementedException();
        }

        public Animal GetById(long id)
        {
            throw new NotImplementedException();
        }

        public void Update(Animal entity)
        {
            throw new NotImplementedException();
        }
    }

}