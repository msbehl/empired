﻿using Empired.Games.GuessTheAnimal.IRepository;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empired.Games.GuessTheAnimal.Tests.Repository
{
    [TestFixture]
    public class RepositoryTests
    {   
        [SetUp]
        public void Initialize()
        {
            _repository = new AnimalRepository();
        }

        [Test]
        public void DeleteAllAnimalsTest()
        {
            _repository.DeleteAll();

            var animals = _repository.GetAll();
            Assert.IsTrue(animals.Count() == 0);
        }    
            
        [Test]
        public void CreateAnimalTest()
        {
            var newAnimal = new Animal { Name = "Elephant", PhysicalAttribute = "Trunk", Sound = "Trumpeting", Color = "Grey" };
            Assert.AreEqual(newAnimal.AnimalID, 0, "Instance not yet persisted, AnimalID should be 0");

            var insertedCount = _repository.Create(newAnimal);
            Assert.AreEqual(insertedCount, 1);

            var animals = _repository.GetAll();
            var insertedAnimal = animals.Where(x => x.Name == newAnimal.Name && x.PhysicalAttribute == newAnimal.PhysicalAttribute && x.Sound == newAnimal.Sound && x.Color == newAnimal.Color).SingleOrDefault();

            Assert.IsNotNull(insertedAnimal);
            Assert.AreNotEqual(insertedAnimal.AnimalID, 0, "AnimalID of successfully persisted animal instance must not be 0");
        }

        [Test]
        public void RetrieveAnimalByIdTest()
        {
            var animals = _repository.GetAll();
            Assert.IsTrue(animals.Count() > 0);

            var randomAnimalId = new Random().Next(animals.Min(x => x.AnimalID), animals.Max(x => x.AnimalID));
            var animalDetails = _repository.GetById(randomAnimalId);
            Assert.IsNotNull(animalDetails); 
        }

        [Test]
        public void UpdateAnimalTest()
        {
            var animals = _repository.GetAll();
            var randomAnimalId = new Random().Next(animals.Min(x => x.AnimalID), animals.Max(x => x.AnimalID));

            var randomAnimal = animals.Where(x => x.AnimalID == randomAnimalId).Single();
            randomAnimal.Name += "Changed";
            randomAnimal.PhysicalAttribute += "Changed";
            randomAnimal.Sound += "Changed";
            randomAnimal.Color += "Changed";

            _repository.Update(randomAnimal);

            var updatedAnimal = _repository.GetById(randomAnimal.AnimalID);
            Assert.AreNotEqual(updatedAnimal.Name.IndexOf("Changed"), -1);
            Assert.AreNotEqual(updatedAnimal.PhysicalAttribute.IndexOf("Changed"), -1);
            Assert.AreNotEqual(updatedAnimal.Sound.IndexOf("Changed"), -1);
            Assert.AreNotEqual(updatedAnimal.Color.IndexOf("Changed"), -1);
        }

        [Test]
        public void RetrieveAllAnimalsTest()
        {
            var animals = _repository.GetAll();
            Assert.IsNotNull(animals);
        }


        [TearDown]
        public void TearDown()
        {
            //DeleteAllAnimalsTest();
        }

        IRepository<Animal> _repository;
    }
}
