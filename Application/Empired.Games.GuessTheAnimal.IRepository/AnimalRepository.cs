﻿using System.Collections.Generic;
using System.Linq;


namespace Empired.Games.GuessTheAnimal.IRepository
{
    public class AnimalRepository : IRepository<Animal>
    {
        public IEnumerable<Animal> GetAll()
        {
            using (var context = new GuessTheAnimalDbContext())
            {
                return context.Animals.ToList();
            }
        }

        public void DeleteAll()
        {
            using (var context = new GuessTheAnimalDbContext())
            {
                context.Animals.RemoveRange(context.Animals);
                context.SaveChanges();
            }
        }

        public int Create(Animal newAnimal)
        {
            using (var context = new GuessTheAnimalDbContext())
            {
                context.Animals.Add(newAnimal);
                return context.SaveChanges();
            }
        }

        public void Delete(Animal entity)
        {
            using (var context = new GuessTheAnimalDbContext())
            {
                context.Animals.Remove(entity);
            }
        }

        public Animal GetById(long animalId)
        {
            using (var context = new GuessTheAnimalDbContext())
            {
                return context.Animals.Where(x => x.AnimalID == animalId).SingleOrDefault();
            }
        }

        public void Update(Animal updatedAnimal)
        {
            using (var context = new GuessTheAnimalDbContext())
            {
                var original = context.Animals.Find(updatedAnimal.AnimalID);
                if (original != null)
                {
                    // ensure only modified properties are sent to the db
                    context.Entry(original).CurrentValues.SetValues(updatedAnimal);
                    context.SaveChanges();
                }
            }
        }       
    }    
}
