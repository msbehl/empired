﻿using System;
using System.Collections.Generic;

namespace Empired.Games.GuessTheAnimal.IRepository
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T>GetAll();

        void DeleteAll();

        T GetById(Int64 id);

        int Create(T entity);

        void Delete(T entity);

        void Update(T entity);

    }
}
