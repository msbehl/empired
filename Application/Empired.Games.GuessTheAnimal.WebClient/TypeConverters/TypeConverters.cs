using Empired.Games.GuessTheAnimal.IRepository;
using Empired.Games.GuessTheAnimal.WebClient.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Empired.Games.GuessTheAnimal.WebClient.TypeConverters
{
    public static class AnimalTypeConverter
    {
        public static Animal Convert(AnimalViewModel viewModel)
        {
            return new Animal
            {
                AnimalID = viewModel.AnimalID,
                Name = viewModel.Name,
                Color = viewModel.Color,
                PhysicalAttribute = viewModel.PhysicalAttribute,
                Sound = viewModel.Sound
            };
        }
                                                                                                        
        public static AnimalViewModel Convert(Animal animalEntity)
        {
            return new AnimalViewModel
            {
                AnimalID = animalEntity.AnimalID,
                Name = animalEntity.Name,
                Color = animalEntity.Color,
                PhysicalAttribute = animalEntity.PhysicalAttribute,
                Sound = animalEntity.Sound
            };
        }
    }
}