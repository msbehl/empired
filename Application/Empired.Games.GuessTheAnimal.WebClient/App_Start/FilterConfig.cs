﻿using System.Web;
using System.Web.Mvc;

namespace Empired.Games.GuessTheAnimal.WebClient
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
