﻿using Empired.Games.GuessTheAnimal.IRepository;
using Empired.Games.GuessTheAnimal.WebClient.Models;
using System.Web.Mvc;

namespace Empired.Games.GuessTheAnimal.WebClient.Controllers
{
    public class GameController : Controller
    {
        public GameController()
        {
            _repository = new AnimalRepository();

            _viewModel = new GameViewModel(_repository.GetAll());
        }

        public ActionResult Index()
        {
            return View(_viewModel);
        }

        [HttpGet]
        public ActionResult SelectColor()
        {
            return View("SelectColor", _viewModel);
        }

        [HttpPost]
        public ActionResult SelectColor(string selectedColor)
        {
            _viewModel.SelectedColor = selectedColor;

            if (_viewModel.IsMatchFeasible())
            {
                return View("SelectSound", _viewModel);
            }else
            {
                return View("DisplayResult", _viewModel.CalculateAnswer());
            }
        }

 
        [HttpPost]
        [AllowAnonymous]
        public ActionResult SelectSound(string selectedColor, string selectedSound)
        {
            _viewModel.SelectedColor = selectedColor;
            _viewModel.SelectedSound = selectedSound;

            if (_viewModel.IsMatchFeasible())
            {
                return View("SelectPhysicalAttribute", _viewModel);
            }
            else
            {
                return View("DisplayResult", _viewModel.CalculateAnswer());
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult SelectPhysicalAttribute(string selectedColor, string selectedSound, string selectedPhysicalAttribute)
        {
            _viewModel.SelectedColor = selectedColor;
            _viewModel.SelectedSound = selectedSound;
            _viewModel.SelectedPhysicalAttribute = selectedPhysicalAttribute;

            return View("DisplayResult", _viewModel.CalculateAnswer());
        }

        [HttpGet]
        public ActionResult AddAnimal()
        {
            return View("AddAnimal", new AnimalViewModel());
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult AddAnimal(AnimalViewModel newAnimal)
        {
            if (ModelState.IsValid)
            {
                _repository.Create(TypeConverters.AnimalTypeConverter.Convert(newAnimal));
                
                return RedirectToAction("Index");
            }

            return View("AddAnimal", newAnimal);
        }

        IRepository<Animal> _repository;
        GameViewModel _viewModel;
    }
}