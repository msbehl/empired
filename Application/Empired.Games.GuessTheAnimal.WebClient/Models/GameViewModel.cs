﻿using Empired.Games.GuessTheAnimal.IRepository;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Empired.Games.GuessTheAnimal.WebClient.Models
{
    public class GameViewModel
    {
        #region Data Binding Properties

        public IList<AnimalViewModel> Animals
        {
            get { return _animals; }
        }

        public IEnumerable<SelectListItem> Colors
        {
            get { return new SelectList(Animals.Select(x => x.Color)); }
        }

        public IEnumerable<SelectListItem> Sounds
        {
            get { return new SelectList(Animals.Select(x => x.Sound)); }
        }

        public IEnumerable<SelectListItem> PhysicalAttributes
        {
            get { return new SelectList(Animals.Select(x => x.PhysicalAttribute)); }
        }

        #endregion

        #region Track User Selections

        public string SelectedColor { get; set; }

        public string SelectedSound { get; set; }

        public string SelectedPhysicalAttribute { get; set; }

        #endregion

        #region Private Properties
        bool IsColorSelected
        {
            get { return !string.IsNullOrEmpty(SelectedColor); }
        }

        bool IsSoundSelected
        {
            get { return !string.IsNullOrEmpty(SelectedSound); }
        }

        bool IsPhysicalAttributeSelected
        {
            get { return !string.IsNullOrEmpty(SelectedPhysicalAttribute); }
        }

        #endregion

        #region Public Properties

        // Check & prevent indeterministic cases
        public bool IsMatchFeasible()
        {
            bool result = true;

            if (IsColorSelected)
            {
                result = _animals.Where(x => x.Color == SelectedColor).SingleOrDefault() != null ? true : false;
            }

            if (IsSoundSelected)
            {
                if (IsColorSelected)
                {
                    result = _animals.Where(x => x.Color == SelectedColor && x.Sound == SelectedSound).SingleOrDefault() != null ? true : false;
                }
                else
                {
                    result = _animals.Where(x => x.Sound == SelectedSound).SingleOrDefault() != null ? true : false;
                }
            }

            return result;
        }

        #endregion

        // Add TypeConverers to cleanly separate DbEntities and Domain Types, Time Permitting
        public GameViewModel(IEnumerable<Animal> animalEntities)
        {
            _animals = new List<AnimalViewModel>();
            if (animalEntities != null)
            {
                foreach (var animalEntity in animalEntities)
                {
                    _animals.Add(new AnimalViewModel { AnimalID = animalEntity.AnimalID, Name = animalEntity.Name, Color = animalEntity.Color, PhysicalAttribute = animalEntity.PhysicalAttribute, Sound = animalEntity.Sound });
                }
            }
        }

        public AnimalViewModel CalculateAnswer()
        {
            AnimalViewModel result = default(AnimalViewModel);

            if (_animals != null && _animals.Count() > 0)
            {
                result = _animals.Where(x => x.Color == SelectedColor
                                        && x.Sound == SelectedSound
                                        && x.PhysicalAttribute == SelectedPhysicalAttribute)
                                        .SingleOrDefault();
            }

            return result;
        }


        IList<AnimalViewModel> _animals;
    }
}