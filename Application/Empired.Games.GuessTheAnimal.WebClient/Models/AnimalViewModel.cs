﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Empired.Games.GuessTheAnimal.WebClient.Models
{
    public class AnimalViewModel
    {
        public int AnimalID { get; set; }

        [Display(Name="Animal Name")]
        [Required(AllowEmptyStrings =false, ErrorMessage ="Please enter a valid animal name")]
        public string Name { get; set; }

        [Display(Name="Animal's Physical Attribute")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter a valid physical attribute")]
        public string PhysicalAttribute { get; set; }


        [Display(Name="Animal's Sound")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter a valid animal sound")]
        public string Sound { get; set; }

        [Display(Name="Animal's Color")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter a valid animal color")]
        public string Color { get; set; }        
    }
}